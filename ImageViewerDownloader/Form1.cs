﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageViewer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void loadImage(object sender, EventArgs e)
        {
            try
            {
                pictureBox1.LoadAsync(textBox_Url.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid url. Try another one.");
            }
           

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

       private void pictureBox1_LoadCompleted_1(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
                MessageBox.Show("Invalid url. Try another one.");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button_SelectDownloadFolder_Click(object sender, EventArgs e)
        {
            string url = textBox_Url.Text;
            int start = url.LastIndexOf("/") + 1;
            int end = url.Length - start;
            string name = url.Substring(start, end);

            int start2 = url.LastIndexOf(".");
            int end2 = url.Length - start2;
            string extension = url.Substring(start2, end2);

            Console.WriteLine(name);
            Console.WriteLine(extension);

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            sfd.FileName = name + "";
            sfd.Filter = extension + " |" + extension;

            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string path = sfd.FileName;
                WebClient webClient = new WebClient();
                
                
                webClient.DownloadFile(url, path);
            }

        }
    }
}
